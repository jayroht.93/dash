// todos filters
export const filters = {
  all: function (todos) {
    return todos
  },
  active: function (todos) {
    return todos.filter(function (todo) {
      return !todo.completed
    })
  },
  completed: function (todos) {
    return todos.filter(function (todo) {
      return todo.completed
    })
  }
}

export class category {
  constructor(options){
    this.description = options.description
    this.name = `_${options.description}_`
    this.icon = options.icon || ''
    this.color = options.color || ''
    this.image = options.image || ''
    this.active = options.active || false

    const d = new Date
    this.date =`${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`
  }
}
