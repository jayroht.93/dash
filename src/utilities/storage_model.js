export const storage = {
  fetch: function (key) {
    let todos=JSON.parse(localStorage.getItem(key) || '[]');
    return todos;
  },
  save: function (key,data) {
    localStorage.setItem(key, JSON.stringify(data))
  }
}
