export const background={
      	'collection':'https://source.unsplash.com/collection/1120243/1920x1080',
        'image':'https://source.unsplash.com/9LpQTxZDaI8/1920x1080',
        'color': 'rgba(0,0,0,0.2)',
        'size': 'cover',
        'repeat':'no-repeat',
        'position':'center'
}
export const themes=[
        {'name':'is-light',
         'description':'Light',
         'inverted':'is-dark',
         'contrast':'is-primary',
         'active':true
        },
        {'name':'is-dark',
         'inverted':'is-light',
         'contrast':'is-primary',
         'description':'Dark',
         'active':false
        }
]

export const todoCategories=[
  {
    "name": "_home_",
    "description": "Home",
    "icon": "home",
    "color":"hsl(171, 100%, 41%)",
    "image":"",
    "active":true
  },
  {
    "name": "_work_",
    "description": "Work",
    "icon": "work",
    "color":"hsl(160, 100%, 41%)",
    "image":"",
    "active":false
  }
]
